var Url = "http://104.236.122.55/doctum/pw/tp1/route.php";
var viagem = {};
//Funçao para puxar tudo da tabela viagem
function Todos(){
    $.ajax({
        type: "GET",
        url: Url,
        dataType: 'json',
        headers: {
            "Content-Type":"application/json",
            "table": "viagem"
        }
    }).success(function(data){
    	console.log(data);
       atualizarTabelaViagem(data);
	});

}


function atualizarTabelaViagem(viagem){
	
	var tabela = document.getElementById('dados-viagem');
	tabela.innerHTML = "";

	for (var i = 0; i<viagem.length;i++){
		var linha=document.createElement("tr");
		var cid=document.createElement("td");
		var cmotorista=document.createElement("td");
		var ccarro=document.createElement("td");
		var csaida=document.createElement("td");
		var cretorno=document.createElement("td");
		var cfuncionarios=document.createElement("td");
		var cBotoes=document.createElement("td");
		
		var vid = document.createTextNode(viagem[i].id);
		var vmotorista = document.createTextNode(viagem[i].motorista__nome);
		var vcarro = document.createTextNode(viagem[i].carro__modelo);
		var vsaida = document.createTextNode(viagem[i].saida);
		var vretorno = document.createTextNode(viagem[i].retorno);
		
		if (viagem[i].funcionarios.length !==0 ){
			var func_nome= [];
			for (let f = 0; f<viagem[i].funcionarios.length; f++){
				func_nome.push(viagem[i].funcionarios[f].funcionario__nome);
			}
		} else {
			func_nome="";
		}
		var vfuncionarios= document.createTextNode(func_nome);

	
		cid.appendChild(vid);
		cmotorista.appendChild(vmotorista);
		ccarro.appendChild(vcarro);
		csaida.appendChild(vsaida);
		cretorno.appendChild(vretorno);
		cfuncionarios.appendChild(vfuncionarios);

		linha.appendChild(cid);
		linha.appendChild(cmotorista);
		linha.appendChild(ccarro);
		linha.appendChild(csaida);
		linha.appendChild(cretorno);
		linha.appendChild(cfuncionarios);
		linha.appendChild(cBotoes);
		tabela.appendChild(linha);
 		
 		cBotoes.innerHTML = "<button class='btn btn-danger btn-sn' onclick='Delete("+viagem[i].id+")'><i class='fas fa-trash'></i></button>" +
       						"<button class='btn btn-warning btn-sn' onclick='editar("+viagem[i].id+")'><i class='fas fa-edit'></i></button>";
}}

function Delete(id){
        if (confirm("Deseja apagar o registro desta viagem, Numero: "+id)) {

            $.ajax({
                type: "DELETE",
                url: Url+"/?id="+id,
                headers: {
                    "Content-Type":"application/json",
                    "table": "viagem"
                }
            }).success(function( data ){
            	console.log(data);
				Todos(data);
			}).error(function( error ){
				console.log(error);
			});

        } else {
            alert("Viagem não excluída.");
        } 
}

