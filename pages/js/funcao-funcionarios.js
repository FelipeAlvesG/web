var Url = "http://104.236.122.55/doctum/pw/tp1/route.php";
var funcionario = {};
//Funçao para puxar tudo da tabela funcionario
function Todos(){
    $.ajax({
        type: "GET",
        url: Url,
        dataType: 'json',
        headers: {
            "Content-Type":"application/json",
            "table": "funcionario"
        }
    }).success(function(data){
       atualizarTabelaFuncionarios(data);
	});

}

function atualizarTabelaFuncionarios(funcionario){
	var tabela = document.getElementById('dados-funcionarios');
	tabela.innerHTML = "";
	console.log(funcionario);
	for (var i = 0; i<funcionario.length;i++){
		var linha=document.createElement("tr");
		var cid=document.createElement("td");
		var cnome=document.createElement("td");
		var cfuncao=document.createElement("td");
		var ctelfixo=document.createElement("td");
		var ctelmovel=document.createElement("td");
		var cemail=document.createElement("td");
		var cBotoes=document.createElement("td");
		
		var vid = document.createTextNode(funcionario[i].id);
		var vnome = document.createTextNode(funcionario[i].nome);
		var vfuncao = document.createTextNode(funcionario[i].funcao);
		var vtelfixo = document.createTextNode(funcionario[i].telefone_fixo);
		var vtelmovel = document.createTextNode(funcionario[i].telefone_movel);
		var vemail = document.createTextNode(funcionario[i].email);

		cid.appendChild(vid);
		cnome.appendChild(vnome);
		cfuncao.appendChild(vfuncao);
		ctelfixo.appendChild(vtelfixo);
		ctelmovel.appendChild(vtelmovel);
		cemail.appendChild(vemail);
		$(cnome).attr('id','editarNome'+funcionario[i].id);
		$(cfuncao).attr('id','editarFuncao'+funcionario[i].id);
		$(ctelfixo).attr('id','editarTelefone'+funcionario[i].id);
		$(ctelmovel).attr('id','editarCelular'+funcionario[i].id);
		$(cemail).attr('id','editarEmail'+funcionario[i].id);
		$(cid).attr('id','editarId'+funcionario[i].id);
     
		linha.appendChild(cid);
		linha.appendChild(cnome);
		linha.appendChild(cfuncao);
		linha.appendChild(ctelfixo);
		linha.appendChild(ctelmovel);
		linha.appendChild(cemail);
		linha.appendChild(cBotoes);
		tabela.appendChild(linha);
		//Criando botoes direto no HTML para EXCLUIR e ALTERAR
       cBotoes.innerHTML = "<button class='btn btn-danger btn-sn' onclick='Delete("+funcionario[i].id+")'><i class='fas fa-trash'></i></button>" +
       "<button class='btn btn-warning btn-sn' onclick='editarFuncionario("+funcionario[i].id+")'><i class='fas fa-edit'></i></button>";
}}
var vData = "";
//Função para alterar o cadastro
function editarFuncionario(id) {
	
    $("#salvar").css({"visibility": "visible",
                        "float": "right" });

	idFunc = "editarId" + id;
    nomeId = "editarNome" + id;
    funcaoId = "editarFuncao" + id;
    telefoneId = "editarTelefone" + id;
    celularId = "editarCelular" + id;
    emailId = "editarEmail" + id;

    var nome = document.getElementById(nomeId).innerHTML;
    var funcao = document.getElementById(funcaoId).innerHTML;
    var telefone = document.getElementById(telefoneId).innerHTML;
    var celular = document.getElementById(celularId).innerHTML;
    var email = document.getElementById(emailId).innerHTML;


	document.getElementById(idFunc).innerHTML = '<input type="hidden" class="form-control" id="idFunc" value="' + id + '">';
    document.getElementById(nomeId).innerHTML = '<input type="text" class="form-control" id="nomeId" value="' + nome + '">';
    document.getElementById(funcaoId).innerHTML = '<input type="text" class="form-control" id="funcaoId" value="' + funcao + '">';
    document.getElementById(telefoneId).innerHTML = '<input type="text" class="form-control" id="telefoneId" value="' + telefone + '">';
    document.getElementById(celularId).innerHTML = '<input type="text" class="form-control" id="celularId" value="' + celular + '">';
    document.getElementById(emailId).innerHTML = '<input type="text" class="form-control" id="emailId" value="' + email + '">';   
}

function salvar(){  
      var vData = JSON.stringify({ 
        id: $("#idFunc").val(),
        nome: $("#nomeId").val(), 
        funcao: $("#funcaoId").val(), 
        telefone_movel: $("#telefoneId").val(), 
        telefone_fixo: $("#celularId").val(),
        email: $("#emailId").val()

    });
      console.log(vData);

  $.ajax({

            type: "POST",
            url: Url+"/?id="+ $("#idFunc").val(),
            data: vData,
            headers: {'table': 'funcionario'},

        }).success( function (data) {
        	console.log(data);
            Todos(data);
        }).error( function (error) {
            console.log(error);
        });

};
                
//Funcao de deleção por ID
function Delete(id){
        if (confirm("Deseja apagar o registro deste funcionario, Numero: "+id)) {

            $.ajax({
                type: "DELETE",
                url: Url+"?id="+id,
                headers: {
                    "Content-Type":"application/json",
                    "table": "funcionario"
                }
            }).success(function( data ){
				Todos(data);
			}).error(function( error ){
				console.log(error);
			});

        } else {
            alert("Funcionario não excluido.");
        }
}