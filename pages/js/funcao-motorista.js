var Url = "http://104.236.122.55/doctum/pw/tp1/route.php";
var motorista = {};
//Funçao para puxar tudo da tabela motorista
function Todos(){
    $.ajax({
        type: "GET",
        url: Url,
        dataType: 'json',
        headers: {
            "Content-Type":"application/json",
            "table": "motorista"
        }
    }).success(function(data){
       atualizarTabelaMotorista(data);
	});

}

function atualizarTabelaMotorista(motorista){
	var tabela = document.getElementById('dados-motorista');
	tabela.innerHTML = "";
	console.log(motorista);
	for (var i = 0; i<motorista.length;i++){
		var linha=document.createElement("tr");
		var cid=document.createElement("td");
		var cnome=document.createElement("td");
		var ctelfixo=document.createElement("td");
		var ctelmovel=document.createElement("td");
		var cemail=document.createElement("td");
		var cBotoes=document.createElement("td");
		
		var vid = document.createTextNode(motorista[i].id);
		var vnome = document.createTextNode(motorista[i].nome);
		var vtelfixo = document.createTextNode(motorista[i].telefone_fixo);
		var vtelmovel = document.createTextNode(motorista[i].telefone_movel);
		var vemail = document.createTextNode(motorista[i].email);

		cid.appendChild(vid);
		cnome.appendChild(vnome);
		ctelfixo.appendChild(vtelfixo);
		ctelmovel.appendChild(vtelmovel);
		cemail.appendChild(vemail);
		$(cnome).attr('id','editarNome'+motorista[i].id);
		$(ctelfixo).attr('id','editarTelefone'+motorista[i].id);
		$(ctelmovel).attr('id','editarCelular'+motorista[i].id);
		$(cemail).attr('id','editarEmail'+motorista[i].id);
		$(cid).attr('id','editarId'+motorista[i].id);


		linha.appendChild(cid);
		linha.appendChild(cnome);
		linha.appendChild(ctelfixo);
		linha.appendChild(ctelmovel);
		linha.appendChild(cemail);
		linha.appendChild(cBotoes);
		tabela.appendChild(linha);

		cBotoes.innerHTML = "<button class='btn btn-danger btn-sn' onclick='Delete("+motorista[i].id+")'><i class='fas fa-trash'></i></button>" +
       "<button class='btn btn-warning btn-sn' onclick='editarMotorista("+motorista[i].id+")'><i class='fas fa-edit'></i></button>";
}}
var vData = "";
//Função para alterar o cadastro
function editarMotorista(id) {
	
    $("#salvar").css({"visibility": "visible",
                        "float": "right" });

	idFunc = "editarId" + id;
    nomeId = "editarNome" + id;
    telefoneId = "editarTelefone" + id;
    celularId = "editarCelular" + id;
    emailId = "editarEmail" + id;

    var nome = document.getElementById(nomeId).innerHTML;
    var telefone = document.getElementById(telefoneId).innerHTML;
    var celular = document.getElementById(celularId).innerHTML;
    var email = document.getElementById(emailId).innerHTML;


	document.getElementById(idFunc).innerHTML = '<input type="hidden" class="form-control" id="idFunc" value="' + id + '">';
    document.getElementById(nomeId).innerHTML = '<input type="text" class="form-control" id="nomeId" value="' + nome + '">';
    document.getElementById(telefoneId).innerHTML = '<input type="text" class="form-control" id="telefoneId" value="' + telefone + '">';
    document.getElementById(celularId).innerHTML = '<input type="text" class="form-control" id="celularId" value="' + celular + '">';
    document.getElementById(emailId).innerHTML = '<input type="text" class="form-control" id="emailId" value="' + email + '">';   
}

function salvar(){  
      var vData = JSON.stringify({ 
        id: $("#idFunc").val(),
        nome: $("#nomeId").val(), 
        telefone_movel: $("#telefoneId").val(), 
        telefone_fixo: $("#celularId").val(),
        email: $("#emailId").val()

    });
      console.log(vData);

  $.ajax({

            type: "POST",
            url: Url+"/?id="+ $("#idFunc").val(),
            data: vData,
            headers: {'table': 'motorista'},

        }).success( function (data) {
        	console.log(data);
            Todos(data);
        }).error( function (error) {
            console.log(error);
        });

};


//Funcao de deleção por ID
function Delete(id){
        if (confirm("Deseja apagar o registro deste motorista, Numero: "+id)) {

            $.ajax({
                type: "DELETE",
                url: Url+"?id="+id,
                headers: {
                    "Content-Type":"application/json",
                    "table": "motorista"
                }
            }).success(function( data ){
				Todos(data);
			}).error(function( error ){
				console.log(error);
			});

        } else {
            alert("Motorista não excluido!!!");
        }
}