var Url = "http://104.236.122.55/doctum/pw/tp1/route.php";
var veiculos = {};
//Funçao para puxar tudo da tabela veiculos
function Todos(){
    $.ajax({
        type: "GET",
        url: Url,
        dataType: 'json',
        headers: {
            "Content-Type":"application/json",
            "table": "veiculo"
        }
    }).success(function(data){
       atualizarTabelaVeiculos(data);
	});

}

function atualizarTabelaVeiculos(veiculos){
	
	var tabela = document.getElementById('dados-veiculo');
	tabela.innerHTML = "";
	console.log(veiculos);
	for (var i = 0; i<veiculos.length;i++){
		var linha=document.createElement("tr");
		var cid=document.createElement("td");
		var cmodelo=document.createElement("td");
		var cqtdeP=document.createElement("td");
		var cativo=document.createElement("td");
		var cBotoes=document.createElement("td");

		var vid = document.createTextNode(veiculos[i].id);
		var vmodelo = document.createTextNode(veiculos[i].modelo);
		var vqtdeP = document.createTextNode(veiculos[i].qtde_passageiros);
		var vativo = document.createTextNode(veiculos[i].ativo);

		
		
		cid.appendChild(vid);
		cmodelo.appendChild(vmodelo);
		cqtdeP.appendChild(vqtdeP);
		cativo.appendChild(vativo);
		$(cmodelo).attr('id','editarModelo'+veiculos[i].id);
		$(cqtdeP).attr('id','editarqtdPess'+veiculos[i].id);
		$(cativo).attr('id','editarAtivo'+veiculos[i].id);
		$(cid).attr('id','editarId'+veiculos[i].id);
     
		
		linha.appendChild(cid);
		linha.appendChild(cmodelo);
		linha.appendChild(cqtdeP);
		linha.appendChild(cativo);
		linha.appendChild(cBotoes);
		tabela.appendChild(linha);

		//Criando botoes direto no HTML para EXCLUIR e ALTERAR
       cBotoes.innerHTML = "<button class='btn btn-danger btn-sn' onclick='Delete("+veiculos[i].id+")'><i class='fas fa-trash'></i></button>" +
       "<button class='btn btn-warning btn-sn' onclick='editarVeiculo("+veiculos[i].id+")'><i class='fas fa-edit'></i></button>";
	}
}

var vData = "";
//Função para alterar o cadastro
function editarVeiculo(id) {
	
    $("#salvar").css({"visibility": "visible",
                        "float": "right" });

	idVei = "editarId" + id;
    modeloId = "editarModelo" + id;
    ativoId = "editarAtivo" +id;
    qtdeId = "editarqtdPess" + id;

    var modelo = document.getElementById(modeloId).innerHTML;
    var qtde_passageiros = document.getElementById(qtdeId).innerHTML;
    var ativo= document.getElementById(ativoId).innerHTML;


	document.getElementById(idVei).innerHTML = '<input type="hidden" class="form-control" id="idVei" value="' + id + '">';
    document.getElementById(modeloId).innerHTML = '<input type="text" class="form-control" id="modeloId" value="' + modelo + '">';
    document.getElementById(ativoId).innerHTML = '<select id="ativoId" required="required"><option value="'+ativo+'">Ativo</option><option value="'+ativo+'">Inativo</option>'+
												'</select>';
    document.getElementById(qtdeId).innerHTML = '<input type="text" class="form-control" id="qtdeId" value="' + qtde_passageiros + '">';
}

function salvar(){  
      var vData = JSON.stringify({ 
        id: $("#idVei").val(),
        modelo: $("#modeloId").val(), 
        ativo:$("ativoId").val(),
        qtde_passageiros: $("#qtdeId").val()
    });
      console.log(vData);

  $.ajax({

            type: "POST",
            url: Url+"/?id="+ $("#idVei").val(),
            data: vData,
            headers: {'table': 'veiculo'},

        }).success( function (data) {
        	console.log(data);
        	Todos(data);
        }).error( function (error) {
            console.log(error);
        });

};


//Funcao de deleção por ID
function Delete(id){
        if (confirm("Deseja apagar o registro deste veiculo, Numero: "+id)) {

            $.ajax({
                type: "DELETE",
                url: Url+"?id="+id,
                headers: {
                    "Content-Type":"application/json",
                    "table": "veiculo"
                }
            }).success(function( data ){
				Todos(data);
			}).error(function( error ){
				console.log(error);
			});

        } else {
            alert("Veiculo não excluido.");
        } 
}